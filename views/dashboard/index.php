
<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\DetailView;
use yii\grid\GridView;
use yii\widgets\Pjax;

$this->title = "Dashboard";
?>

<div class="row">
	<div class="col-lg-3 col-xs-6">
		<!-- small box -->
		<div class="small-box bg-aqua">
			<div class="inner">
			  <h3><?= $countPros ?></h3>

			  <p> <?= Html::a('<span class="bg-aqua">All Professionals</span>', Url::to(['professional/list'])  ); ?>  </p>
			</div>
			<div class="icon">
			  <i class="ion ion-bag"></i>
			</div>
		</div>
	</div>
	<!-- ./col -->
	<div class="col-lg-3 col-xs-6">
		<!-- small box -->
		<div class="small-box bg-green">
			<div class="inner">
			  <h3><?= $countCust ?><sup style="font-size: 20px"></sup></h3>

      <p><?= Html::a('<span class="bg-green">Customers</span>', Url::to(['customer/list'])  ); ?> </p> 
			  
			</div>
			<div class="icon">
			  <i class="ion ion-stats-bars"></i>
			</div>
		</div>
	</div>
	<!-- ./col -->
	<div class="col-lg-3 col-xs-6">
		<!-- small box -->
		<div class="small-box bg-yellow">
			<div class="inner">
			  <h3><?= $requests ?> </h3>

			  <p><?= Html::a('<span class="bg-yellow">All Requests</span>',Url::to(['request/list']) ); ?></p>
			</div>
			<div class="icon">
			  <i class="ion ion-person-add"></i>
			</div>
		</div>
	</div>
	<!-- ./col -->
	<div class="col-lg-3 col-xs-6">
		<!-- small box -->
		<div class="small-box bg-red">
			<div class="inner" >
			  <h3><?= $services ?> </h3>

			  <p ><?= Html::a('<span class="bg-red">Services</span>',  Url::to(['services/list'])  ); ?> </p>
			</div>
			<div class="icon">
			  <i class="ion ion-pie-graph"></i>
			</div>
		</div>
	</div>
	<!-- ./col -->
</div>
