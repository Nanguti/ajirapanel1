<?php
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\DetailView;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\bootstrap\Modal;

//models
use app\models\common\Storage;
?>
    <p>
        <?= Html::a('Back', ['list'], ['class' => 'btn btn-primary']) ?>
    </p>

<section class="content-header">
	<h1> <?php	$this->title = "Customer Details ({$data['customer']['iUserId']})"; ?> </h1>
</section>
  <?php
    yii\bootstrap\Modal::begin(['id' =>'modal','size'=>'modal-lg',]);
    yii\bootstrap\Modal::end();

  ?>
<div class="box body_font">
	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box-body">
					<table id="example2" class="table table-bordered table-hover">
						<tbody>
						<?php
							echo DetailView::widget([
								'model' => $data['customer'],
								'attributes' => [
									[
										'label' => 'ID',
										'attribute' => 'iUserId',
										'format' => 'raw',
									],
									[
										'label' => 'First Name',
										'attribute' => 'vFirstName',
										'format' => 'raw',
									],
									[
										'label' => 'Last Name',
										'attribute' => 'vLastName',
										'format' => 'raw',

									],
									[
										'label' => 'DOB',
										'attribute' => 'dob',
										'format' => 'raw',
									],
									[
										'label' => 'Mobile Country Code',
										'attribute' => 'mobileCountryCode',
										'format' => 'raw',
									],
									[
										'label' => 'Email',
										'attribute' => 'vEmail',
										'format' => 'raw',
									],
									[
										'label' => 'Gender',
										'attribute' => 'gender',
										'format' => 'raw',
									],
									[
										'label' => 'Latitude',
										'attribute' => 'vLatitude',
										'format' => 'raw',
									],
									[
										'label' => 'Longitude',
										'attribute' => 'vLongitude',
										'format' => 'raw',
									],
									[
										'label' => 'User Photo',
										'attribute' => 'vUserPhoto',
										'format' => 'raw',
									],
									[
										'label' => 'C.Date',
										'attribute' => 'cdate',
										'format' => 'raw',
									],
									[
										'attribute'=>'location',
										'label'=>'Request Location',
									],
									[
										'label'=>'Request GPS Location',
										'format' => 'raw',
										'value'=>function($data){
											return Html::a('<i class="fas fa-map-marked"></i>',"http://www.google.com/maps/place/{$data['vLatitude']},{$data['vLongitude']}",['target'=>'_blank']);
										},
									],
									
								],
							]);
						?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</section>
</div>
<div class="box body_font">
	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box-body">
					<h3> Details</h3>
					<table id="example2" class="table table-bordered table-hover">
						<tbody>
							<?php
								echo DetailView::widget([
									'model' => $data['details'],
									'attributes' => [
										'occupation',
										'website',
										'education',
										'experience',
										'aboutYourself',
										'bio',
									],
								]);
						?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</section>
</div>
<div class="box body_font">
	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box-body">
					<h3> Status</h3>
					<table id="example2" class="table table-bordered table-hover">
						<tbody>
							<?php
								echo DetailView::widget([
									'model' => $data['status_count'],
									'attributes' => [
											[
										'label' => 'C.Date',
										'attribute' => 'cdate',
										'format' => 'raw',
									],
										[
										'label' => 'U.Date',
										'attribute' => 'udate',
										'format' => 'raw',
									],
										'cancelledRequests',
										'acceptedRequests',
										'pendingRequests',
										'progressRequests',
										'deletedRequests',
										'completedRequests',
										'unavailableRequests',
									],
								]);
						?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</section>
</div>
