<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\grid\GridView;
use yii\widgets\Pjax;

$this->title = 'Customers';
?>
<p>
        <?= Html::a('Emails', ['email'], ['class' => 'btn btn-primary']) ?>
    </p>
<div class="box body_font">
	<div class="row">
		<div class="col-xs-12">
			<div class="box-body">
				<table id="example2" class="table table-bordered table-hover">
					<tbody>
					<?php
						echo GridView::widget([
							'dataProvider' => $data,
							'filterModel' => $model,
							'id' => 'requestlistgridview',
							'options'=>['class'=>'table-responsive'],
							'columns' => [
								[
									'label' => 'ID',
									'attribute' => 'iUserId',
									'format' => 'raw',
									'value'=>function($data){
										return Html::a($data['iUserId'],Url::to(['customer/detail']),['data-method'=>'POST','data-params'=>['iUserId'=>$data['iUserId']], 'target'=>'_blank']);
									},
								],
								[
									'label' => 'First Name',
									'attribute' => 'vFirstName',
									'format' => 'raw',
								],
								[
									'label'=> 'Last Name',
									'attribute'=>'vLastName',
									'format'=> 'raw',
								],
								[
									'label'=> 'Number',
									'attribute'=>'mobileNumber',
									'format'=>'raw',
								],
								[
									'label'=>'Email',
									'attribute'=> 'vEmail',
									'format'=> 'raw',
								],
								[
									'label'=>'DoB',
									'attribute'=>'dob',
									'format'=>'raw',
								],
								[
									'label'=>'Gender',
									'attribute'=>'gender',
									'format'=>'raw',
								],
								[
									'label'=>'Status',
									'attribute'=>'eStatus',
									'filter'=> array(""=>"All", "Active"=>"Active", "Inactive"=>"Inactive", "Blocked"=>"Blocked", "Deleted"=> "Deleted"),
								],
							],
						]);
					?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
