<?php
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\DetailView;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\bootstrap\Modal;

//models
use app\models\common\Storage;
?>

<section class="content-header">
	<h1> <?php	$this->title = "Request Details ({$data['request']['job_id']})"; ?> </h1>
</section>

<div class="box body_font">
	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box-body">
					<table id="example2" class="table table-bordered table-hover">
						<tbody>
						<?php
							echo DetailView::widget([
								'model' => $data['request'],
								'attributes' => [
									'job_date',
									'customerId',
									'customerPhone',
									'customerName',
									'customerStatus',
									'categoryName',
									'serviceName',
									[
										'attribute'=>'location',
										'label'=>'Request Location',
									],
									[
										'label'=>'Request GPS Location',
										'format' => 'raw',
										'value'=>function($data){
											return Html::a('<i class="fas fa-map-marked"></i>',"http://www.google.com/maps/place/{$data['latitude']},{$data['longitude']}",['target'=>'_blank']);
										},
									],
									[
										'attribute'=>'status',
										'label'=>'Request Status',
									],
								],
							]);
						?>
						<br>
						<br>
						<br>
						<div class="box">
							<div class="box-body">
								<?php
									echo GridView::widget([
										'dataProvider' => $data['questions'],
										'id' => 'detailQuestions',
										'options'=>['class'=>'table-responsive'],
										'columns' => [
											[
												'label' => 'Question',
												'attribute' => 'question',
												'format' => 'raw',
											],
											[
												'label' => 'Answer',
												'attribute' => 'answer',
												'format' => 'raw',
												'value'=>function($data){
													if($data['type']=='Images' && is_array($data['answer'])){
														$temp = '';
														foreach($data['answer'] as $image) $temp .= Html::a(Html::img($image,['width'=>100]),$image);
														return $temp;
													}else{
														return $data['answer'];
													}
												},
											],
										],
									]);
								?>
							</div>
						</div>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</section>
</div>

<div class="box body_font">
	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box-body">
					<table id="example2" class="table table-bordered table-hover">
						<tbody>
						<?php
							echo GridView::widget([
								'dataProvider' => $data['providers'],
								'id' => 'detailProviders',
								'columns' => [
									[
										'label' => 'UID',
										'attribute' => 'uid',
										'format' => 'raw',
									],
									[
										'label' => 'Name',
										'attribute' => 'proName',
										'format' => 'raw',
									],
									[
										'label' => 'Type',
										'attribute' => 'eBizType',
										'format' => 'raw',
									],
									[
										'label' => 'Number',
										'attribute' => 'mobileNumber',
										'format' => 'raw',
									],
									[
										'label' => 'Location',
										'attribute' => 'location',
										'format' => 'raw',
									],
																		[
										'label' => 'Message',
										'attribute' => 'message',
										'format' => 'raw',
									],
									[
										'label' => 'Quote',
										'attribute' => 'cost',
										'format' => 'raw',
									],
									[
										'label' => 'PCommision',
										'attribute' => 'commision',
										'format' => 'raw',
									],
									[
										'label' => 'CCommision',
										'attribute' => 'commision_customer',
										'format' => 'raw',
									],
									[
										'label' => 'Quote Date',
										'attribute' => 'quote_date',
										'format' => 'raw',
									],
									[
										'label' => 'Status',
										'attribute' => 'status',
										'format' => 'raw',
									],
								],
							]);
						?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</section>
</div>
