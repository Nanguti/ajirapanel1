<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\bootstrap\Modal;
use app\models\request\Request;
use yii\jui\DatePicker;

$this->title = 'Request';
?>
<div class="box body_font">
	<div class="row">
		<div class="col-xs-12">
			<div class="box-body">
				<table id="example2" class="table table-bordered table-hover">
					<tbody>
						<?php
						echo GridView::widget([
							'dataProvider' => $data,
							'filterModel' => $model,
							'id' => 'requestlistgridview',
							'options'=>['class'=>'table-responsive'],
							'columns' => [
								[
									'label' => 'Job ID',
									'attribute' => 'job_id',
									'format' => 'raw',
									'value'=>function($data){
										return Html::a($data['job_id'],Url::to(['request/detail']),['data-method'=>'POST','data-params'=>['id'=>$data['id']], 'target'=>'_blank']);
									},
								],
								[
									'label' => 'Customer Name',
									'attribute' => 'customerName',
									'format' => 'raw',
								],
								[
									'label' => 'Service',
									'attribute' => 'serviceName',
									'format' => 'raw',
								],
								[
									'label' => 'Date',
									'attribute' => 'cdate',
					 				'filter' => DatePicker::widget(['language' => 'en', 'dateFormat' => 'yyyy-MM-dd',
									'clientOptions' => [
                                    'changeYear' => true,
                                    'changeMonth' => true,
                                    'minDate' => '2018-01-01',
                                    'maxDate' => date('Y-m-d',strtotime('0 years')),
									]]),
								],
								[
									'label' => 'Status',
									'attribute' => 'status',
									'filter'=>array(""=>"All","Cancelled"=>"Cancelled", "Completed"=>"Completed", "Deleted"=>"Deleted", "Hired"=>"Hired", "Pending"=>"Pending", "Rejected"=>"Rejected", "Timeout"=>"Timeout"),
								],
							],
						]);
						?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
