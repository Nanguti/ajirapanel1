<?php
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\DetailView;
use yii\grid\GridView;
use yii\widgets\Pjax;
?>



<div class="row">
	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
		<?php
			$this->title = "Professional Details ({$data['professional']['uid']})"; 
			$proStatus = $data['professional']['professionalStatus'];
			$proId= $data['professional']['uid'];
			$lat = $data['professional']['vLatitude'];
			$long = $data['professional']['vLongitude'];
		
			$ch = curl_init(); 
			curl_setopt($ch, CURLOPT_URL, "https://maps.googleapis.com/maps/api/geocode/json?latlng=$lat,$long&sensor=false&key=AIzaSyD8P2u06oAhLVvLvqa0SSS2qR1KBIJnFIk" ); 
			curl_setopt($ch, CURLOPT_POST, 1 ); 
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_HTTPHEADER, array(
			'Content-Type: application/json',
			'Content-Length: 0'));
			$postResult = curl_exec($ch);
			curl_close($ch);
			
			$location = json_decode($postResult)->results;
			if (!empty($location)){
			$address = $location['0']->formatted_address;
			echo $address;
			}
			else {
				echo "Pro location not available";
			}
		?>
	</div>
	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
		<div class="toggleswitch">
			<input type="checkbox" name="toggleswitch" class="toggleswitch-checkbox" id="myonoffswitch" <?= ($proStatus == 'Active')? 'checked' :'' ?> onclick= "proStatus(<?= $proId ?>)">
			<label class="toggleswitch-label" for="myonoffswitch">
				<span class="toggleswitch-inner"></span>
				<span class="toggleswitch-switch"></span>
			</label>
		</div>
	</div>
</div>


<?php if(Yii::$app->session->hasFlash('status')){ ?>

<div class="alert alert-success alert-dismissible">
	<?= Yii::$app->session->getFlash('status'); ?>
</div>

<?php } ?> 

<div class="box body_font">
	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box-body">
					<table id="example2" class="table table-bordered table-hover">
						<tbody>
						<?php

							echo DetailView::widget([
								'model' => $data['professional'],
								'attributes' => [
									[
										'label'=> 'Phone Number',
										'attribute'=>'professionalPhone',
										'format'=>'raw',
									],
									[
										'label'=>'Email',
										'attribute'=>'vEmail',
										'format'=>'raw',
									],
									[
										'label'=>'Date of Birth',
										'attribute'=>'dob',
										'format'=>'raw',
									],
									[
										'label'=>'Business Name',
										'attribute'=>'businessName',
										'format'=>'raw',
									],
									[
										'label'=>'Id Number',
										'attribute'=>'id_no',
										'format'=>'raw',
									],
									[
										'label'=>'KRA Number',
										'attribute'=>'revenue_authority_no',
										'format'=>'raw',
									],									
										'business_reg_no',
										'conduct_cert_no',
									[
										'label'=>'Mobile Verified?',
										'attribute'=>'eMobileVerified',
									],
									[
										'label'=>'Business Type',
										'attribute'=>'eBizType',
									],
										'cdate',
										'approval_status',
										'gender',
										'professionalStatus',
									[
										'label'=>'Request GPS Location',
										'format' => 'raw',
										'value'=>function($data){
											return Html::a('<i class="fas fa-map-marked"></i>',"http://www.google.com/maps/place/{$data['vLatitude']},{$data['vLongitude']}",['target'=>'_blank']);
										},
									],
									'waitingRequests',
									'deletedRequests',
									'completedRequests',
									'rejectedRequests',
									'timedoutRequests',
								],
							]);
						?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</section>
</div>
<div class="box body_font">
	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box-body">
					<h3> Services
					<?php
					$count = count($data['services']);
					echo "(" . $count . ")";
					?>
					</h3>
					<table id="example2" class="table table-bordered table-hover">
						<tbody>
						<tr>
							<th> Id </th>
							<th> Category </th>
							<th> Subcategory</th>
							<th> Service Name </th>
							<th> Date </th>
							<?php
							$number = 1;
							foreach($data['services'] as $service){
								echo "<tr> <td>" . $number++ . "</td><td>" . $service['category'] . "</td> <td>" . $service['subcategory'] . "</td> <td>" .  $service['serviceName'] . "</td> <td>".  $service['cdate'] . "</td></tr>";
							}
							?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</section>
</div>
<div class="box body_font">
	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box-body">
					<h3> Details</h3>
					<table id="example2" class="table table-bordered table-hover">
						<tbody>
							<?php
								echo DetailView::widget([
									'model' => $data['details'],
									'attributes' => [
										'occupation',
										'website',
										'education',
										'experience',
										'aboutYourself',
										'bio',
									],
								]);
						?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</section>
</div>
<div class="box body_font">
	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box-body">
					<h3> Documents</h3>
					<table id="example2" class="table table-bordered table-hover">
						<tbody>
						<?php
							echo DetailView::widget([
								'model' => $data['documents'],
								'attributes' => [
									[
										'label' => 'ID Proof',
										'attribute' => 'id_proof',
										'format' => 'raw',
										'value'=>function($data){
											if(!empty($data['id_proof'])){
												return Html::a('ID Proof',$data['id_proof']);
											}
											return;
										},
									],
									[
										'label' => 'Conduct Cert',
										'attribute' => 'conduct_cert',
										'format' => 'raw',
										'value'=>function($data){
											if(!empty($data['conduct_cert'])){
												return Html::a('Conduct Cert',$data['conduct_cert']);
											}
											return;
										},
									],
									[
										'label' => 'Industry Cert',
										'attribute' => 'industry_cert',
										'format' => 'raw',
										'value'=>function($data){
											if(!empty($data['industry_cert'])){
												return Html::a('Industry Cert',$data['industry_cert']);
											}
											return;
										},
									],
									[
										'label' => 'Academic Cert',
										'attribute' => 'academic_cert',
										'format' => 'raw',
										'value'=>function($data){
											if(!empty($data['academic_cert'])){
												return Html::a('Academic Cert',$data['academic_cert']);
											}
											return;
										},
									],
									[
										'label' => 'Incorporation Cert',
										'attribute' => 'incorporation_cert',
										'format' => 'raw',
										'value'=>function($data){
											if(!empty($data['incorporation_cert'])){
												return Html::a('Incorporation Cert',$data['incorporation_cert']);
											}
											return;
										},
									],
									[
										'label' => 'Revenue Authority Cert',
										'attribute' => 'revenue_authority_cert',
										'format' => 'raw',
										'value'=>function($data){
											if(!empty($data['revenue_authority_cert'])){
												return Html::a('Revenue Authority Cert',$data['revenue_authority_cert']);
											}
											return;
										},
									],
									[
										'label' => 'Bank Cert',
										'attribute' => 'bank_cert',
										'format' => 'raw',
										'value'=>function($data){
											if(!empty($data['bank_cert'])){
												return Html::a('Bank Cert',$data['bank_cert']);
											}
											return;
										},
									],
									[
										'label' => 'Permit Cert',
										'attribute' => 'permit_cert',
										'format' => 'raw',
										'value'=>function($data){
											if(!empty($data['permit_cert'])){
												return Html::a('Permit Cert',$data['permit_cert']);
											}
											return;
										},
									],
									[
										'label' => 'Industry Awards Cert',
										'attribute' => 'industry_awards_cert',
										'format' => 'raw',
										'value'=>function($data){
											if(!empty($data['industry_awards_cert'])){
												return Html::a('Industry Awards Cert',$data['industry_awards_cert']);
											}
											return;
										},
									],
								],
							]);
						?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</section>
</div>
<script>

function proStatus(id){

	if (result) {
	$.ajax({
		type:'POST',
		async:true,
		dataType:'json',
		url:'<?= Yii::$app->urlManager->createUrl(['professional/activate']) ?>/'+id,
	}).done(function(){

	});
			window.location.reload();
	}
}

</script>