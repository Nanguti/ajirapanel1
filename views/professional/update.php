<?php
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\DetailView;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\widgets\ActiveForm;

?>

<div class="content-wrapper">

  <!-- Main content -->
    <section class="content">
		<div class="row">
			<div class="col-md-6">
				<div class="box box-primary">
					<form role="form">
						<div class="box-body">
							<?php $form = ActiveForm::begin(); ?>
							<div class="form-group">
							<?= $form->field($model, 'vFirstName')->textInput(['value'=>$data['professional']['vFirstName']]) ?>
							</div>
							<div class="form-group">
							<?= $form->field($model, 'vLastName')->textInput(['maxlength' => true, 'value'=>$data['professional']['vLastName']]) ?>
							</div>
							<div class="form-group">
							<?= $form->field($model, 'businessName')->textInput(['maxlength' => true, 'value'=>$data['professional']['businessName']]) ?>
							</div>
							<div class="form-group">
							<?= $form->field($model, 'vEmail')->textInput(['maxlength' => true, 'value'=>'' ]) ?>
							</div>
							<div class="form-group">
							<?= $form->field($model, 'mobileCountryCode')->textInput(['maxlength' => true, 'value'=>'' ]) ?>
							</div>
							<div class="form-group">
							<?= $form->field($model, 'id_no')->textInput(['maxlength' => true, 'value'=>'' ]) ?>
							</div>
							<div class="form-group">
							<?= $form->field($model, 'revenue_authority_no')->textInput(['maxlength' => true, 'value'=>'' ]) ?>
							</div>
							<div class="form-group">
							<?= $form->field($model, 'business_reg_no')->textInput(['maxlength' => true, 'value'=>'' ]) ?>
							</div>
							<div class="form-group">
							<?= $form->field($model, 'conduct_cert_no')->textInput(['maxlength' => true, 'value'=>'' ]) ?>
							</div>
						</div>
						<div class="col text-center">
							<div class="form-group">
							<?= Html::submitButton('Save', ['class' => 'btn btn-warning']) ?>
							</div>
						</div>
							<?php ActiveForm::end(); ?>		  
					</form>
				</div>
			</div>
		</div>
    </section>
 </div>




