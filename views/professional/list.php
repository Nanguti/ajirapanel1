<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\bootstrap\Modal;

$this->title = 'Professionals';
?>
<div class="box body_font">
	<div class="row">
		<div class="col-xs-12">
			<div class="box-body">
				<table id="example2" class="table table-bordered table-hover">
					<tbody>
					<?php
						yii\bootstrap\Modal::begin(['id' =>'modal']);
						yii\bootstrap\Modal::end();
						echo GridView::widget([
							'dataProvider' => $data,
							'filterModel' => $model,
							'id' => 'requestlistgridview',
							'options'=>['class'=>'table-responsive'],
							'columns' => [
 								[
									'label' => 'ID',
									'attribute' => 'iUserId',
									'format' => 'raw',
									'value'=>function($data){
										return Html::a($data['iUserId'],Url::to(['professional/detail']),['data-params'=>['iUserId'=>$data['iUserId']],'data-method'=>'POST','class'=>'popupModall', 'target'=> '_blank']);
									},
								],   
								[
									'label' => 'First Name',
									'attribute' => 'vFirstName',
									'format' => 'raw',
								],
								[
									'label' => 'Last Name',
									'attribute' => 'vLastName',
									'format' => 'raw',
								],
								[
									'label' => 'Email',
									'attribute' => 'vEmail',
									'format' => 'raw',
								],
								[
									'label' => 'Mobile Number',
									'attribute' => 'mobileNumber',
									'format' => 'raw',
								],
								[
									'label'=>'Approval Status',
									'attribute'=>'approval_status',
									'filter'=> array(""=>"All", "Waiting documents"=>"Waiting documents", "Verify"=>"Verify"),
								],
								[
									'label'=>'Status',
									'attribute'=>'eStatus',
									'filter'=> array(""=>"All", "Active"=>"Active", "Inactive"=>"Inactive", "Blocked"=>"Blocked", "Deleted"=>"Deleted"),
								],								
							],
							
						]);
						
					?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
<?php
$this->registerJs("$(function() {
   $('.popupModal').click(function(e) {
     e.preventDefault();
     $('#modal').modal('show').find('.modal-content')
     .load($(this).attr('value'));
   });
});");
?>
