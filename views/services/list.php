<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\grid\GridView;
use yii\widgets\Pjax;

$this->title = 'Services';
?>

<div class="box body_font">
	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box-body">
					<table id="example2" class="table table-bordered table-hover">
						<tbody>
						<?php
							echo GridView::widget([
								'dataProvider' => $data,
								'filterModel' => $model,
								'id' => 'serviceslistgridview',
								'options'=>['class'=>'table-responsive'],
								'columns' => [
									[
										'label'=> 'ID',
										'attribute'=> 'iCategoryMasterId',
										'format'=>'raw',
									],
									[
										'label' => 'Category',
										'attribute' => 'Category',
										'format' => 'raw',
									],
									[
										'label' => 'Service Name',
										'attribute' => 'serviceName',
										'format' => 'raw',
									],
																		[
										'label' => 'Date',
										'attribute' => 'cdate',
										'format' => 'raw',
									],
																		[
										'label' => 'Status',
										'attribute' => 'eStatus',
										'filter'=> array(""=>"All", "Active"=>"Active", "Inactive"=>"Inactive"),
									],
								],
							]);
						?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</section>
</div>
