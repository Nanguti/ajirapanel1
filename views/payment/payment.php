<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\grid\GridView;
use yii\widgets\Pjax;

$this->title = 'Payment';
?>
<div class="box body_font">
	<div class="row">
		<div class="col-xs-12">
			<div class="box-body">
				<table id="example2" class="table table-bordered table-hover">
					<tbody>
					<?php
					    yii\bootstrap\Modal::begin(['id' =>'modal']);
						yii\bootstrap\Modal::end();
 					 		echo GridView::widget([
							'dataProvider' => $payment,
							'filterModel' => $model,
							'id' => 'requestlistgridview',
							'options'=>['class'=>'table-responsive'],
							'columns' => [
/* 								[
									'label' => 'ID',
									'attribute' => 'id',
									'format' => 'raw',
 									'value'=>function($payment){
										return Html::a('More', Url::to(['payment/detail', 'id'=>$payment->details]),['id' => 'popupModal']);
									}, 
								], */
								[
									'label' => 'ID',
									'attribute' => 'id',
									'format' => 'raw',
								],
								[
									'label' => 'First Name',
									'attribute' => 'vFirstName',
									'format' => 'raw',
								],
								[
									'label' => 'Last Name',
									'attribute' => 'vLastName',
									'format' => 'raw',
								],
								[
									'label' => 'Customer Number',
									'attribute' => 'mobileNumber',
									'format' => 'raw',
								],
								[
									'label' => 'Ref.',
									'attribute' => 'reference',
									'format' => 'raw',
								],
								[
									'label' => 'Amount',
									'attribute' => 'amount',
									'format' => 'raw',
								],
								[
									'label' => 'Status',
									'attribute' => 'status',
									'format' => 'raw',
								],
								[
									'label' => 'Transactional Date',
									'attribute' => 'cdate',
									'format' => 'raw',
								],
							],
						]);  
					?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
<?php
$this->registerJs("$(function() {
   $('.popupModal').click(function(e) {
     e.preventDefault();
     $('#modal').modal('show').find('.modal-content')
     .load($(this).attr('href'));
   });
});");
?>