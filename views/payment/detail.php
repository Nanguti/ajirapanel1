<?php
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\DetailView;
use yii\grid\GridView;
use yii\widgets\Pjax;
?>
<?php
$this->title = 'Payment Details';
?>
<div class="box body_font">
	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box-body">
					<h3> Details</h3>
					<table id="example2" class="table table-bordered table-hover">
						<tbody>
							<?php
								echo DetailView::widget([
									'model' => $data,
									'attributes' => [
										'transID',
										'transactionType',
										'transAmount',
										'transTime',
										'businessShortCode',
										'billRefNumber',
										'invoiceNumber',
										'thirdPartyTransID',
										'phone',
										'firstName',
										'middleName',
										'lastName',
										'orgAccountBalance',
									],
								]);
							?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</section>
</div>