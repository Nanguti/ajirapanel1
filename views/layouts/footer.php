<?php
use yii\helpers\Html;

?>
<footer class="main-footer body_font">
    <div class="pull-right hidden-xs">
		<?= number_format((memory_get_usage()/1000), 0, '.', '') ?>Kb - <?= number_format(Yii::getLogger()->getElapsedTime(), 2, '.', '') ?>s - <b>Version</b> 2.0
    </div>
    <strong>Copyright &copy; 2019 <a href="http://ajiraconnect.com">Ajira Connect</a>.</strong> All rights
    reserved.
</footer>
