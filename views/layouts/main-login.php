<?php
use app\assets\AppAsset;
use yii\helpers\Html;
AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
	<head>
		<meta charset="<?= Yii::$app->charset ?>"/>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<?= Html::csrfMetaTags() ?>
		<title><?= Html::encode($this->title) ?></title>
		<?php $this->head() ?>
		<link rel="icon" href="/favicon.ico">
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
		<link href="<?= Yii::getAlias('@web') ?>/css/AdminLTE.css" rel="stylesheet">
		<link href="<?= Yii::getAlias('@web') ?>/css/skins/_all-skins.css" rel="stylesheet">
		<link href="<?= Yii::getAlias('@web') ?>/css/site.css" rel="stylesheet">
	</head>
	<body class="login-page">
		<?php $this->beginBody() ?>
			<?= $content ?>
		<?php $this->endBody() ?>
	<script src="<?= Yii::getAlias('@web') ?>/js/adminlte.min.js"></script>
	</body>
</html>
<?php $this->endPage() ?>
