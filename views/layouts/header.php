<?php
use yii\helpers\Html;
?>


<header class="main-header">
    <?= Html::a('<span class="logo-mini">AC</span><span class="logo-lg">' . Yii::$app->name . '</span>', Yii::$app->homeUrl, ['class' => 'logo']) ?>
    <nav class="navbar navbar-static-top" role="navigation">
        <a href="#" class="sidebar-toggle" data-toggle="push-menu">
            <span class="fas fa-bars"></span>
        </a>
        <div class="navbar-custom-menu dropdown ">
            <ul class="nav navbar-nav ">
                <li class="user user-menu">
                    <ul class="dropdown-menu" role="menu">

                    </ul>
                </li>
                <li>
                  <a href="#" data-toggle="control-sidebar"><i class="fas fa-gears"></i></a>
                </li>
                <li class="user-footer">
                          <?= Html::a(
                              'Sign out',
                              ['/site/logout'],
                              ['data-method' => 'post', 'class' => '']
                          ) ?>
                  </li>
            </ul>
        </div>
    </nav>
</header>
