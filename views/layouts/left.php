<aside class="main-sidebar body_font">
    <section class="sidebar">
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search..."/>
              <span class="input-group-btn">
                <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
            </div>
        </form>
        <?= app\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu tree', 'data-widget'=> 'tree'],
                'items' => [
                    ['label' => 'Main', 'options' => ['class' => 'header']],
					['label' => 'Dashboard', 'icon' => 'user-circle', 'url' => ['/dashboard']],
                    ['label' => 'Request', 'icon' => 'user-circle', 'url' => ['/request']],
                    ['label' => 'Professionals', 'icon' => 'user-circle', 'url' => ['/professional']],
                    ['label' => 'Customers', 'icon' => 'user-md', 'url' => ['/customer']],
                    ['label' => 'Services', 'icon' => 'users', 'url' => ['/services']],
                    ['label' => 'Payment', 'icon' => 'credit-card', 'url' => ['/payment/payment']],
                    ['label' => 'Receipts', 'icon' => 'file', 'url' => ['/request']],
                    ['label' => 'Login', 'url' => ['site/login'], 'visible' => Yii::$app->user->isGuest],
                ],
            ]
        ) ?>
    </section>
</aside>
