<?php
namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\payment\Payment;
use app\models\payment\PaymentDetail;

class PaymentController extends Controller
{

	public function behaviors()
	{
		return [
			'access' => [
				'class' => AccessControl::className(),
				'rules' => [
					[
						'actions' => ['payment', 'detail'],
						'allow' => true,
						'roles' => ['@'],
					],
				],
			],
		];
	}

	public function beforeAction($action)
	{
		
		if (!parent::beforeAction($action)) return false;
		
		$this->layout = 'main';
		
		return true;

	}
	
	public function actions()
	{
		
		return [
			'error' => [
				'class' => 'yii\web\ErrorAction',
			],
		];
	}
	
	public function actionIndex()
	{
		
		return $this->redirect(['dashboard/index']);
		
	}
	
	public function actionPayment()
	{
		$model = new Payment();
		$data = $model->paymentList();
		return $this->render('payment', ['model'=>$model, 'payment'=>$data]);
	}
	
	public function actionDetail()
	{
		$transID = Yii::$app->request->get('id');
		$model = new PaymentDetail();
		$data = $model->paymentDetail($transID);
		return $this->renderAjax('detail',['model'=>$model,'data'=>$data]);
	}
}