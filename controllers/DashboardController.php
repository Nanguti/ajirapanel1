<?php
namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\Contact;
use app\models\API\API;
use app\models\LoginForm;

class DashboardController extends Controller
{
	public function behaviors()
	{
		return [
			'access' => [
				'class' => AccessControl::className(),
				'rules' => [
					[
						'actions' => ['index'],
						'allow' => true,
						'roles' => ['@'],
					],
				],
			],
			'verbs' => [
				'class' => VerbFilter::className(),
				'actions' => [
					'logout' => ['post'],
				],
			],
		];
	}

	public function beforeAction($action)
	{
		if (!parent::beforeAction($action)) return false;
		$this->layout = 'main';
		return true;
	}

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionIndex()
    {

		function countPros()
		{
 			$api = new API();
			$api->call('professionals/list',[]);
			$count = count($api->response['data']['professionals']);
			return $count; 
		}

		function countCust()
		{
			$api = new API();
			$api->call('customers/list',[]);
			$count = count($api->response['data']['customers']);
			return $count;
		}
		
		function totalRequests()
		{
			$api = new API();
			$api->call('requests/list',[]);
			$count = count($api->response['data']['requests']);
			return $count;
		}
		
		function allServices()
		{
			$api = new API();
			$api->call('services/list',[]);
			$count = count($api->response['data']['services']);
			return $count;
		}
		return $this->render('index',['countPros' => countPros(), 'countCust' => countCust(), 'requests'=> totalRequests(), 'services'=> allServices()]);
    }
}