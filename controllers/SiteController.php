<?php
namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\Contact;
use app\models\API\API;
use app\models\LoginForm;

class SiteController extends Controller
{
	public function behaviors()
	{
		return [
			'access' => [
				'class' => AccessControl::className(),
				'rules' => [
					[
						'actions' => ['login'],
						'allow' => true,
						'roles' => ['?'],
					],
					[
						'actions' => ['index'],
						'allow' => true,
					],
					[
						'actions' => ['logout'],
						'allow' => true,
						'roles' => ['@'],
					],
				],
			],
			'verbs' => [
				'class' => VerbFilter::className(),
				'actions' => [
					'logout' => ['post'],
				],
			],
		];
	}

	public function beforeAction($action)
	{
		if (!parent::beforeAction($action)) return false;
		$this->layout = 'main';
		return true;
	}

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionIndex()
    {

		if(Yii::$app->user->isGuest){
			return $this->redirect(['site/login']);
		}
		return Yii::$app->getResponse()->redirect('dashboard/index');
    }

    public function actionLogin()
    {		
		$this->layout='main-login';
		if(!Yii::$app->user->isGuest){
			
			return $this->redirect(['dashboard/index']);
		}
		$model = new LoginForm();

		if($model->load(Yii::$app->request->post()) && $model->login()){
			return $this->redirect(['dashboard/index']);
		}
		return $this->render('login',['model'=>$model]);
	}

	public function actionLogout()
	{
		Yii::$app->user->logout();
		return $this->redirect(['site/login']);
	}
}