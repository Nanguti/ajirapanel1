<?php
namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\API\API;
use app\models\professional\Professional;
use app\models\professional\ProfessionalDetail;
use app\models\professional\ProfessionalActivate;

class ProfessionalController extends Controller
{

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['list','index','detail', 'activate', 'edit', 'entry'],
                        'allow' => true,
                        
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function beforeAction($action)
    {
        
        if (!parent::beforeAction($action)) return false;
        
        $this->layout = 'main';
        
        return true;

    }
    
    public function actions()
    {
        
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }
    
    public function actionIndex()
    {
        
        return $this->redirect(['professional/list']);
        
    }
    
    public function actionList()
    {
        
        $api = new API();
        $return = $api->call('professionals/list',[]);
        if(!empty($return) && !empty($api->response['settings']['success'])){
            
            $model = new Professional(['scenario'=>'list']);
            
            if($model->load(Yii::$app->request->get()) && $model->validate()){
                $data = $model->professionalList($api->response['data']['professionals']);
            }else{
                $data = $model->professionalList($api->response['data']['professionals']);
				
            }
            
            return $this->render('list',['model'=>$model,'data'=>$data]);
        }
        
        return $this->redirect(['dashboard/index']);
        
    }
	

    public function actionDetail()
    {
        
        $model = new ProfessionalDetail(['scenario'=>'detail']);
        if($model->load(['ProfessionalDetail'=>Yii::$app->request->post()]) && $model->validate()){
            
            $api = new API();
            $return = $api->call('professionals/detail',['iUserId'=>$model->iUserId]);

            if(!empty($return) && !empty($api->response['settings']['success'])){				
                $data = $model->professionalDetail($api->response['data']);	
                return $this->render('detail',['model'=>$model,'data'=>$data]);
            }
        }
		
        return $this->redirect(['dashboard/index']);
        
    }
    
    public function actionActivate($id)
    {
        
        $model = new Professional(['scenario'=>'list']);
        
        $proId = Yii::$app->request->get('id'); 
        $api = new API();
        $api->call('professionals/detail',['uid'=>$proId]); 
        $proStatus = $model->professionalDetail($api->response['data']);
        $status = $proStatus['professional']['professionalStatus'];
        $newStatus = ($status=='Active')?'Inactive':'Active';
        $return = $api->call('professionals/activate',['iUserId'=>$proId, 'eStatus'=>$newStatus]);
        if(!empty($return) && !empty($api->response['settings']['success'])){
            
            Yii::$app->getSession()->setFlash('status', 'Pro Status Changed! ');
                
            $data = $model->professionalDetail($api->response['data']);

            return $this->render('detail',['model'=>$model,'data'=>$data]);
            
        }

        return $this->redirect(['professional/list']);
        
    }   
}