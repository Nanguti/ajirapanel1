<?php
namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\API\API;
use app\models\request\Request;
use app\models\request\RequestDetail;

class RequestController extends Controller
{

	public function behaviors()
	{
		return [
			'access' => [
				'class' => AccessControl::className(),
				'rules' => [
					[
						'actions' => ['list','index','detail'],
						'allow' => true,
						'roles' => ['@'],
					],
				],
			],
		];
	}

	public function beforeAction($action)
	{

		if (!parent::beforeAction($action)) return false;

		$this->layout = 'main';

		return true;

	}

	public function actions()
	{

		return [
			'error' => [
				'class' => 'yii\web\ErrorAction',
			],
		];

	}

	public function actionIndex()
	{

		return $this->redirect(['request/list']);

	}

	public function actionList()
	{

		$api = new API();
		$return = $api->call('requests/list',[]);

		if(!empty($return) && !empty($api->response['settings']['success'])){

			$model = new Request(['scenario'=>'list']);

			if($model->load(Yii::$app->request->get()) && $model->validate()){
				$data = $model->requestList($api->response['data']['requests']);
			}else{
				$data = $model->requestList($api->response['data']['requests']);
			}

			return $this->render('list',['model'=>$model,'data'=>$data]);

		}

		return $this->redirect(['request/request']);

	}

	public function actionDetail()
	{

		$model = new RequestDetail(['scenario'=>'detail']);

		if($model->load(['RequestDetail'=>Yii::$app->request->post()]) && $model->validate()){

			$api = new API();
			$return = $api->call('requests/detail',['id'=>$model->id]);

			if(!empty($return) && !empty($api->response['settings']['success'])){

				$data = $model->requestDetail($api->response['data']);

				return $this->render('detail',['model'=>$model,'data'=>$data]);

			}

		}

		return $this->redirect(['dashboard/index']);

	}
}