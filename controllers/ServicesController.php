<?php
namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\API\API;
use app\models\services\Services;

class ServicesController extends Controller
{

	public function behaviors()
	{
		return [
			'access' => [
				'class' => AccessControl::className(),
				'rules' => [
					[
						'actions' => ['list','index'],
						'allow' => true,
						'roles' => ['@'],
					],
				],
			],
		];
	}
	
	public function beforeAction($action)
	{

		if (!parent::beforeAction($action)) return false;
		$this->layout = 'main';
		return true;

	}

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionIndex()
    {

		return $this->redirect(['services/list']);

    }

    public function actionList()
    {

        $api = new API();
        $return = $api->call('services/list',[]);

        if(!empty($return) && !empty($api->response['settings']['success'])){

			$model = new Services(['scenario'=>'list']);

			if($model->load(Yii::$app->request->get()) && $model->validate()){

				$data = $model->servicesList($api->response['data']['services']);

			}else{

				$data = $model->servicesList($api->response['data']['services']);

			}

            return $this->render('list',['model'=>$model,'data'=>$data]);

        }

		return $this->redirect(['dashboard/index']);

    }
   
   	public function actionDeleteService()
	{
		$this->findModel($id)->delete();
        return $this->redirect(['index']);
	}

}