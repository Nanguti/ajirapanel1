<?php
namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\API\API;
use app\models\customer\Customer;
use app\models\customer\CustomerDetail;

class CustomerController extends Controller
{
	public function behaviors()
	{
		return [
			'access' => [
				'class' => AccessControl::className(),
				'rules' => [
					[
						'actions' => ['list','index','edit','detail','email'],
						'allow' => true,
						'roles' => ['@'],
					],
				],
			],
		];
	}
	
	public function beforeAction($action)
	{

		if (!parent::beforeAction($action)) return false;
		$this->layout = 'main';
		return true;

	}

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }
	
    public function actionIndex()
    {
		return $this->redirect(['customer/list']);
    }

    public function actionList()
    {
        $api = new API();
        $return = $api->call('customers/list',[]);
		
        if(!empty($return) && !empty($api->response['settings']['success'])){
			
			$model = new Customer(['scenario'=>'list']);
			
			if($model->load(Yii::$app->request->get()) && $model->validate()){
				$data = $model->customerList($api->response['data']['customers']);
			}else{
				$data = $model->customerList($api->response['data']['customers']);
			}
			
            return $this->render('list',['model'=>$model,'data'=>$data]);

        }
		
		return $this->redirect(['dashboard/index']);
    }

    public function actionDetail()
	{

		$model = new CustomerDetail(['scenario'=>'detail']);

		if($model->load(['CustomerDetail'=>Yii::$app->request->post()]) && $model->validate()){
			$api = new API();
			$return = $api->call('customers/detail',['iUserId'=>$model->iUserId]);

			if(!empty($return) && !empty($api->response['settings']['success'])){
				
				$data = $model->customerDetail($api->response['data']);

				return $this->render('detail',['model'=>$model,'data'=>$data]);
			}
		}

		return $this->redirect(['dashboard/index']);

	}
	    public function actionEmail()
    {
        $api = new API();
        $return = $api->call('customers/list',[]);
		
        if(!empty($return) && !empty($api->response['settings']['success'])){
			
			$model = new Customer(['scenario'=>'list']);
			
			if($model->load(Yii::$app->request->get()) && $model->validate()){
				$data = $model->customerList($api->response['data']['customers']);
			}else{
				$data = $model->customerList($api->response['data']['customers']);
			}
			
            return $this->render('email',['model'=>$model,'data'=>$data]);

        }
		
		return $this->redirect(['dashboard/index']);
    }
	
}