<?php
namespace app\models\payment;

use Yii;
use yii\base\Model;
use yii\data\ArrayDataProvider;

//models


class PaymentDetail extends Model
{

	public $firstName;
	public $middleName;
	public $lastName;
	public $invoiceNumber;
	public $billRefNumber;
	public $businessShortCode;
	public $transTime;
	public $transAmount;
	public $transID;
	public $transactionType;
	public $thirdPartyTransID;
	public $phone;
	public $orgAccountBalance;
	public $uid;

    public function rules()
	{
		return [

			//detail
			['uid', 'integer', 'on'=>'detail'],
			['firstName', 'string'],
			['middleName', 'string'],
			['lastName', 'string'],
			['invoiceNumber', 'string'],
			['billRefNumber', 'string'],
			['businessShortCode', 'string'],
			['transTime', 'string'],
			['transAmount', 'string'],
			['transID', 'string'],
			

		];
	}
	
	Public function paymentDetail($transID)
	{
		$data = json_decode($transID);
		//$data = array($data)[0];
		//$data = ($data)->body;
		$data = $data->Body->stkCallback->CallbackMetadata;
		//$data = $data->Item;
		//$data = $data->Item[4];
		
		//$data = $data[0];
		print_r($data);
		exit;
		
		return $data;
	}
	
	public function attributeLabels()
    {
        return [
            'vFirstName' => 'First Name',
            'vLastName' => 'Last Name',
            'businessName' => 'Business Name',
            'vEmail' => 'Email',
            'bussregno' => 'Bussiness Reg no',

        ];
    }

	private function filterLikeArray($item,$search,$data,$caseSensitive=0)
	{

		if(!is_array($this->$item) && !is_array($search) && $search && $this->$item){
			foreach($data as $i=>$v){
				if($caseSensitive){
					if(strpos($v[$item],$search)===false){
						unset($data[$i]);
					}
				}else{
					if(strpos(strtolower($v[$item]),strtolower($search))===false){
						unset($data[$i]);
					}
				}
			}
		}

		return $data;

	}
}