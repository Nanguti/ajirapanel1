<?php
namespace app\models\payment;

use Yii;
use yii\base\Model;
use yii\data\ArrayDataProvider;

class Payment extends Model
{

	public $id;
	public $vFirstName;
	public $vLastName;
	public $mobileNumber;
	public $reference;
	public $status;
	public $amount;
	public $cdate;

    public function rules()
    {
        return [
			['id', 'trim','on'=>'list'],
			
			['vFirstName', 'trim','on'=>'list'],
			['vFirstName', 'string', 'length' => [0,6],'on'=>'list'],
			
			['vLastName', 'trim', 'on'=>'list'],
			['vLastName', 'string', 'length' => [0,6],'on'=>'list'],
			
			['mobileNumber', 'trim', 'on'=>'list'],
			['mobileNumber', 'string', 'length' => [0,6],'on'=>'list'],
			
			['reference', 'trim','on'=>'list'],
			['reference', 'string', 'length' => [0,20],'on'=>'list'],
			
			['status', 'trim','on'=>'list'],
			['status', 'string', 'length' => [0,20],'on'=>'list'],
			
			['cdate', 'trim','on'=>'list'],
			['cdate', 'string', 'length' => [0,20],'on'=>'list'],
			//detail
			['id', 'integer', 'on'=>'detail'],

        ];
    }

    public function paymentList()
    {
/* 		$data = array( 'id' => '29');
		
		$testData = json_encode($data); */
		
 		$ch = curl_init(); 
		curl_setopt($ch, CURLOPT_URL, Yii::$app->params['apiurl']['2']."list/deposits" ); 
		curl_setopt($ch, CURLOPT_POST, 1 ); 
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
		//curl_setopt($ch, CURLOPT_POSTFIELDS, $testData);
/* 		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
		'Content-Type: application/json',
		'Content-Length: ' . strlen($testData))
		); */
		$postResult = curl_exec($ch);
		$payment = json_decode($postResult)->data;
		//$payment = [$pay];
		//print_r($payment);
		//exit;
		curl_close($ch);
		
		$payment = $this->filterLikeArray('id',$this->id, $payment);
		$payment = $this->filterLikeArray('vFirstName',$this->vFirstName,$payment);
		$payment = $this->filterLikeArray('vLastName',$this->vLastName,$payment);
		$payment = $this->filterLikeArray('mobileNumber',$this->mobileNumber,$payment);
		$payment = $this->filterLikeArray('amount',$this->amount, $payment);
		$payment = $this->filterLikeArray('reference',$this->reference, $payment);
		$payment = $this->filterLikeArray('status',$this->status, $payment);
		$payment = $this->filterLikeArray('cdate',$this->cdate, $payment);

		$arrayDataProvider = new ArrayDataProvider([
			'allModels'=>$payment,
			'pagination'=>[
				'pageSize'=>Yii::$app->params['pagination'],
			],
			'sort' => [
				'attributes'=>[
					'id',
					'vFirstName',
					'vLastName',
					'mobileNumber',
					'amount',
					'reference',
					'status',
					'cdate'
				],
				'defaultOrder'=>['id'=>SORT_DESC],
			],
		]);	
		return $arrayDataProvider; 	
    }
	
	private function filterLikeArray($item,$search,$payment,$caseSensitive=0)
	{

		if(!is_array($this->$item) && !is_array($search) && $search && $this->$item){
			foreach($payment as $i=>$v){
				if($caseSensitive){
					if(strpos($v[$item],$search)===false){
						unset($payment[$i]);
					}
				}else{
					if(strpos(strtolower($v[$item]),strtolower($search))===false){
						unset($payment[$i]);
					}
				}
			}
		}

		return $payment;

	}
}
