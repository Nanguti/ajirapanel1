<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * LoginForm is the model behind the login form.
 *
 * @property User|null $user This property is read-only.
 *
 */
class LoginForm extends Model
{
    public $email;
    public $password;
    public $rememberMe = true;

	private $_user = false;


    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
		
            // username and password are both required
			['email', 'required','message'=>'Email or Mobile Number cannot be blank.'],
			['email','trim'],
			['email', 'filter','skipOnArray'=>true,'filter' => 'strtolower'],
			['email', 'string', 'min' => 5, 'max' => 100, 'message'=>'Email or Mobile Number must be beetwen 5 to 100 characters.'],
			
            // rememberMe must be a boolean value
            ['rememberMe','boolean'],
            // password is validated by validateLogin()
			
			['password', 'required', 'message'=>'Please enter a password'],
			['password', 'string', 'min' => 8, 'max' => 32, 'message'=>'Passwords must be 8-32 characters long and should include at least one letter and one number. Numbers, letters and special characters (!@#$%_) are accepted.'],
			['password', 'match', 'pattern' => "/^(?=.*[0-9]+.*)(?=.*[a-zA-Z]+.*)[0-9a-zA-Z!@#$%_]{8,32}$/", 'message' => 'Passwords must be 8-32 characters long and should include at least one letter and one number. Numbers, letters and special characters (!@#$%_) are accepted.'],
            ['password','validateLogin'],
			
        ];
    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
	public function validateLogin($attribute, $params)
    {
		
        if (!$this->hasErrors()) {
            
			$return = User::validateLogin('admin@admin.com','12345678a');
			
            if(empty($return)){

				$this->addError($attribute, 'Incorrect username or password.');

			}
			
			$this->_user = $return;
			
        }
		
    }

    /**
     * Logs in a user using the provided username and password.
     * @return bool whether the user is logged in successfully
     */
    public function login()
    {
		
        if($this->validate()){
			return Yii::$app->user->login($this->_user, $this->rememberMe ? 3600*24*30 : 0);
        }
		
        return false;
		
    }

}