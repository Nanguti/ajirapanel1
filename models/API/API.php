<?php

namespace app\models\API;

use Yii;
use yii\base\Model;

/**
 * LoginForm is the model behind the login form.
 *
 * @property User|null $user This property is read-only.
 *
 */
class API extends Model
{
	
	//to be send
	public $params = [];
	public $call = '';
	
	//privates
	private $callParams = [];
	private $url = '';
	private $calls = [];
	private $method = '';
	private $encode64 = '';
	private $userAgent = '';
	private $connectTimeout = 0;
	private $timeout = 0;
	private $apiKey = '';
	private $apiKeyName = '';
	private $apiToken = '';
	private $apiHash = '';

	//you can get the response after call api
	public $responseJson = '';
	public $response = [];
	public $responseObj;
	
	function __construct()
	{
		
		$this->userAgent = Yii::$app->params['API']['useragent'];
		$this->connectTimeout = Yii::$app->params['API']['connectTimeout'];
		$this->timeout = Yii::$app->params['API']['timeout'];
		
	}
	
    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
		
			//paramss
			[['params'],'required','skipOnEmpty'=>true],
			[['params'],'validateArray','skipOnEmpty'=>true],
			
			//call
			[['call'],'required','on'=>'call'],
			[['call'],'string','min'=>3,'max'=>64],
			[['call'],'in','range'=>$this->calls],
			
        ];
    }

	/*
		array validation
	*/
	public function validateArray($attribute,$params)
	{

		if(empty($this->$attribute) || !is_array($this->$attribute))
			$this->addError($attribute,"$attribute must be array");

	}
	
	/*
	*	befoireValidate
	*	before perform any call reset following variables
	*/
	public function beforeValidate()
	{
		
		$this->responseJson = '';
		$this->response = [];
		$this->responseObj =(object)[];

		$this->method = Yii::$app->params['API']['defaultMethod'];
		$this->encode64 = Yii::$app->params['API']['encode64'];
				
		$this->calls = [];
		foreach(Yii::$app->params['API']['call'] as $i=>$v) $this->calls[] = $i;
		
		return true;

	}

	/*
	*	afterValidate
	*/
	public function afterValidate()
	{
	
		$errors= $this->getErrors();
		if(empty($errors)){

			//get call parameters
			$this->apiKey = Yii::$app->params['API']['accessKey']['key'];
			$this->apiKeyName = Yii::$app->params['API']['accessKey']['name'];
			
			$this->callParams = Yii::$app->params['API']['call'][$this->call];
			
			//get api url
			$this->url = Yii::$app->params['API']['url'][$this->callParams['url']];
			
			//default or custom method
			if(!empty($this->callParams['method'])) $this->method = $this->callParams['method'];
			
			//encode params to base64
			if(isset($this->callParams['encode64'])) $this->encode64 = $this->callParams['encode64'];
			if(!empty($this->encode64)) $this->encodeParams();

		}

	}

	/*
		encode params to base 64
	*/
	private function encodeParams(){
		
		foreach($this->params as $i=>$v) $this->params[$i] = base64_encode($v);
		
	}

	public function call($call,$params){
		
		//set
		$this->call = $call;
		$this->params = $params;
		
		//validate
		if($this->validate()){
			
			//call api
			return $this->curlCall();
			
		}

		return $this->getErrors();
		
	}
	
	private function curlCall(){
		
		if($this->method=='POST'){
			
			$responseJson = $this->curlPost();
			$response = json_decode($responseJson,true);
			$responseObj = json_decode($responseJson);

			if(!empty($response) && !empty($responseObj)){
				
				$this->responseJson = $responseJson;
				$this->response = $response;
				$this->responseObj = $responseObj;
				
				return true;
				
			}
			
		}elseif($this->method=='GET'){
		
			$responseJson = $this->curlGet();
			$response = json_decode($responseJson,true);
			$responseObj = json_decode($responseJson);

			if(!empty($response) && !empty($responseObj)){
				
				$this->responseJson = $responseJson;
				$this->response = $response;
				$this->responseObj = $responseObj;
				
				return true;
				
			}
		
		}
		
		return false;
		
	}
	
	/*
		call curl using post method
	*/
	private function curlPost(){
		
		$this->params = array_merge($this->params, $this->prepareHashToken());

		$curl= curl_init();

		curl_setopt_array($curl,[
			CURLOPT_RETURNTRANSFER=>true,
			CURLOPT_SSL_VERIFYPEER=>false,
			CURLOPT_URL=>"{$this->url}{$this->call}",
			CURLOPT_USERAGENT=>$this->userAgent,
			CURLOPT_POST=>true,
			CURLOPT_POSTFIELDS=>$this->params,
			CURLOPT_CONNECTTIMEOUT=>$this->connectTimeout,
			CURLOPT_TIMEOUT=>$this->timeout,
		]);
		
		$resp= curl_exec($curl);
		
		curl_close($curl);
		
		return $resp;
		
	}

	/*
		call curl using get method
	*/
	private function curlGet(){
		
		$params = $this->prepareGetParams();
		
		$curl= curl_init();
		
		curl_setopt_array($curl,[
			CURLOPT_RETURNTRANSFER=>true,
			CURLOPT_SSL_VERIFYPEER=>false,
			CURLOPT_URL=>"{$this->url}{$this->call}?{$params}",
			CURLOPT_USERAGENT=>$this->userAgent,
			CURLOPT_CONNECTTIMEOUT=>$this->connectTimeout,
			CURLOPT_TIMEOUT=>$this->timeout,
		]);

		$resp= curl_exec($curl);

		curl_close($curl);
		
		return $resp;
		
	}

	/*
		prepare get parameters (encoding)
	*/
	private function prepareGetParams(){
		
		$getParams = '';
		
		foreach($this->params as $i=>$v){
			$value = urlencode($v);
			$getParams.="{$i}={$value}&";
		} 
		
		return rtrim($getParams,'&');
		
	}

	/*
		prepare hash and token validation
	*/
	private function prepareHashToken(){
		
		$rand = $hash = $token ='';

		
		//hash
		if(!empty($this->callParams['validateAccessKey'])){

			$rand = $this->generateRand();
			$hash = $this->apiKeyName . hash('sha512',hash('sha512',$rand).hash('sha512',$this->apiKey).hash('sha512',$rand.$this->call.gmdate("mY",time())));
			
		}

		//token
		if(!empty($this->callParams['validateToken'])){
			
			$session = Yii::$app->session;
			
			if(!empty($session->get('token'))){
				
				$token = $session->get('token');
		
			}
		}
		
		return ['hash'=>$hash,'rand'=>$rand,'token'=>$token];
		
	}
	
	// generate rand
    private static function generateRand($digits=40){

		$i = 0;
		$code = ''; 
		while($i<$digits){
			$code .= (string)mt_rand(0, 9);
			$i++;
		}
		
		return sha1($code);

    }
	
}