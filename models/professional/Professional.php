<?php
namespace app\models\professional;

use Yii;
use yii\base\Model;
use yii\data\ArrayDataProvider;

class Professional extends Model
{

	public $vFirstName;
	public $vLastName;
	public $vEmail;
	public $gender;
	public $mobileNumber;
	public $id_no;
	public $iUserId;
	public $eStatus;
	public $approval_status;

    public function rules()
    {
        return [

			//list
			['iUserId','integer','on'=>'list'],

			['vFirstName', 'trim','on'=>'list'],
			['vFirstName', 'string', 'length'=>[0,128], 'on'=>'list'],
			
			['vFirstName', 'trim','on'=>'list'],
			['vLastName', 'string', 'length' => [0,128],'on'=>'list'],

			['vEmail', 'trim','on'=>'list'],
			['vEmail', 'string', 'length' => [0,128],'on'=>'list'],

			['gender', 'trim','on'=>'list'],
			['gender', 'string', 'length' => [0,6],'on'=>'list'],

			['mobileNumber', 'trim','on'=>'list'],
			['mobileNumber', 'string', 'length' => [0,10],'on'=>'list'],

			['id_no', 'trim', 'on'=>'list'],
			['id_no', 'string', 'length'=>[0,8], 'on'=>'list'],

			['eStatus', 'trim', 'on'=>'list'],
			['eStatus', 'string', 'length'=>[0,8], 'on'=>'list'],

			['approval_status', 'trim', 'on'=>'list'],
			['approval_status', 'string', 'length'=>[0,32], 'on'=>'list'],

			//detail
			['iUserId', 'integer', 'on'=>'detail'],

        ];
    }

    public function professionalList($data)
    {

		$data = $this->filterLikeArray('iUserId',$this->iUserId, $data);
		$data = $this->filterLikeArray('vFirstName',$this->vFirstName,$data);
		$data = $this->filterLikeArray('vLastName',$this->vLastName,$data);
		$data = $this->filterLikeArray('vEmail',$this->vEmail,$data);
		$data = $this->filterLikeArray('id_no',$this->id_no, $data);
		$data = $this->filterLikeArray('mobileNumber',$this->mobileNumber, $data);
		$data = $this->filterLikeArray('eStatus',$this->eStatus, $data);
		$data = $this->filterLikeArray('approval_status',$this->approval_status, $data);


		$arrayDataProvider = new ArrayDataProvider([
			'allModels'=>$data,
			'pagination'=>[
				'pageSize'=>Yii::$app->params['pagination'],
			],
			'sort' => [
				'attributes'=>[
					'vFirstName',
					'vLastName',
					'vEmail',
					'id_no',
					'iUserId', 
					'mobileNumber', 
					'eStatus',
					'approval_status'
				],
				'defaultOrder'=>['iUserId'=>SORT_DESC],
			],
		]);
		return $arrayDataProvider;

    }
	
    public function professionalDetail($data)
    {

		$arrayDataProvider['professional'] = new ArrayDataProvider([
			'allModels'=>$data['professional'],
			'pagination'=>[
				'pageSize'=>Yii::$app->params['pagination'],
			],
		]);
    }

	private function filterLikeArray($item,$search,$data,$caseSensitive=0)
	{

		if(!is_array($this->$item) && !is_array($search) && $search && $this->$item){
			foreach($data as $i=>$v){
				if($caseSensitive){
					if(strpos($v[$item],$search)===false){
						unset($data[$i]);
					}
				}else{
					if(strpos(strtolower($v[$item]),strtolower($search))===false){
						unset($data[$i]);
					}
				}
			}
		}

		return $data;

	}

}