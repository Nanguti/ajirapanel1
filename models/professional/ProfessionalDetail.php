<?php
namespace app\models\professional;
use app\models\API\API;

use Yii;
use yii\base\Model;
use yii\data\ArrayDataProvider;

//models


class ProfessionalDetail extends Model
{

	public $vFirstName;
	public $vLastName;
	public $businessName;
	public $vEmail;
	public $mobileCountryCode;
	public $dob;
	public $gender;
	public $id_no;
	public $revenue_authority_no;
	public $business_reg_no;
	public $conduct_cert_no;
	public $eMobileVerified;
	public $eBizType;
	public $fAvgRating;
	public $vUserPhoto;
	public $available;
	public $cdate;
	public $tPendingReason;
	public $approval_status;
	public $professionalPhone;
	public $professionalStatus;
	public $cancelledRequests;
	public $hiredRequests;
	public $pendingRequests;
	public $waitingRequests;
	public $deletedRequests;
	public $completedRequests;
	public $rejectedRequests;
	public $timedoutRequests;
	public $documents;
	public $iUserId;
	public $phoneNumber;
	public $proId;
	public $uid;

	

    /**
     * @return array the validation rules.
     */
    public function rules()
	{
		return [

			//detail
			['iUserId', 'integer', 'on'=>'detail'],
			['vFirstName', 'string'],
			['vLastName', 'string'],
		];
	}

    public function professionalDetail($data)
	{
		$arrayDataProvider['professional'] = $data['professional'];
		$arrayDataProvider['documents'] = $data['documents'];
		$arrayDataProvider['details'] = $data['details'];
		$arrayDataProvider['services'] = $data['services'];
		return $arrayDataProvider;		
	}
	
	public function attributeLabels()
    {
        return [
            'vFirstName' => 'First Name',
            'vLastName' => 'Last Name',
            'businessName' => 'Business Name',
            'vEmail' => 'Email',
            'bussregno' => 'Bussiness Reg',

        ];
    }

	private function filterLikeArray($item,$search,$data,$caseSensitive=0)
	{

		if(!is_array($this->$item) && !is_array($search) && $search && $this->$item){
			foreach($data as $i=>$v){
				if($caseSensitive){
					if(strpos($v[$item],$search)===false){
						unset($data[$i]);
					}
				}else{
					if(strpos(strtolower($v[$item]),strtolower($search))===false){
						unset($data[$i]);
					}
				}
			}
		}

		return $data;

	}
}
