<?php
namespace app\models\professional;

use Yii;
use yii\base\Model;
use yii\data\ArrayDataProvider;

class ProfessionalActivate extends Model
{

	public $iUserId;
	public $eStatus;

    public function rules()
	{
		return [

			//activate
			['iUserId', 'required', 'message' => 'Please provide a valid pro id.', 'on' => 'activate'],
			['iUserId','integer','on' => 'activate'],	
			['eStatus', 'required', 'message' => 'Please provide the pro status.', 'on' => 'activate'],
			// ['eStatus','string','on' => 'activate'],	

		];
	}

    public function professionalActivate($data)
	{
		$update = new ProfessionalActivate();
		
		$update->eStatus = 'Inactive';
		$update->iUserId = $this->iUserId;

		$update->save(0);	

		return true;
	}
}
