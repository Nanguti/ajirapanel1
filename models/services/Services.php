<?php
namespace app\models\services;

use Yii;
use yii\base\Model;
use yii\data\ArrayDataProvider;

class Services extends Model
{

	public $Category;
	public $iCategoryMasterId;
	public $serviceName;
	public $cdate;
	public $eStatus;

    public function rules()
    {
        return [

			//list
			['iCategoryMasterId', 'trim','on'=>'list'],
			['iCategoryMasterId', 'string', 'length' => [0,128],'on'=>'list'],

			['Category', 'trim','on'=>'list'],
			['Category', 'string', 'length' => [0,128],'on'=>'list'],

			['serviceName', 'trim','on'=>'list'],
			['serviceName', 'string', 'length' => [0,128],'on'=>'list'],
			
			['cdate', 'trim','on'=>'list'],
			['cdate', 'string', 'length' => [0,128],'on'=>'list'],
			
			['eStatus', 'trim','on'=>'list'],
			['eStatus', 'string', 'length' => [0,128],'on'=>'list'],

			//detail

        ];
    }
	
    public function servicesList($data)
    {
		$data = $this->filterLikeArray('Category',$this->Category,$data);
		$data = $this->filterLikeArray('iCategoryMasterId',$this->iCategoryMasterId,$data);
		$data = $this->filterLikeArray('serviceName',$this->serviceName,$data);
		$data = $this->filterLikeArray('cdate',$this->cdate,$data);
		$data = $this->filterLikeArray('eStatus',$this->eStatus,$data);
		$arrayDataProvider = new ArrayDataProvider([
			'allModels'=>$data,
			'pagination'=>[
				'pageSize'=>Yii::$app->params['pagination'],
			],
			'sort' => [
				'attributes'=>[
					'iCategoryMasterId',
					'Category',
					'serviceName',
					'cdate',
					'eStatus',
					],
				'defaultOrder'=>['iCategoryMasterId'=>SORT_DESC],
			],
		]);

		return $arrayDataProvider;

    }

	private function filterLikeArray($item,$search,$data,$caseSensitive=0)
    {

		if(!is_array($this->$item) && !is_array($search) && $search && $this->$item){
			foreach($data as $i=>$v){
				if($caseSensitive){
					if(strpos($v[$item],$search)===false){
						unset($data[$i]);
					}
				}else{
					if(strpos(strtolower($v[$item]),strtolower($search))===false){
						unset($data[$i]);
					}
				}
			}
		}

		return $data;

	}
}