<?php
namespace app\models;

use Yii;

//models
use app\models\API\API;

class User extends \yii\base\BaseObject implements \yii\web\IdentityInterface
{

    public $id;
    public $password;
    public $authKey;
    public $accessToken;
    public $full_name;
    public $email;
    public $status;
    public $phonenumber;
    public $token;
    public $users = [];

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
	{

        $api = new API();
        $return = $api->call('admin/details',[]);

        if(!empty($return) && !empty($api->response['settings']['success'])){

            return isset($api->response['data']) ? new static($api->response['data']) : null;
			
        }
		
        //destroy session if cannot retrieve user
        $session = Yii::$app->session;
        $session->destroy();

        return null;
		
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
	{
        foreach (self::$users as $user) {
            if ($user['accessToken'] === $token) {
                return new static($user);
            }
        }

        return null;
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    /* public static function findByUsername($username)
      {
      foreach (self::$users as $user) {
      if (strcasecmp($user['username'], $username) === 0) {
      return new static($user);
      }
      }

      return null;
      } */

    public static function validateLogin($email, $password)
	{

        $api = new API();

        $return = $api->call('admin/auth',['email'=>'admin@admin.com','password'=>'12345678a']);

        if(!empty($return) && !empty($api->response['settings']['success'])){

            //remove some fields
            unset($api->response['data']['hash']);
            unset($api->response['data']['rand']);

            //set token in session
            $session = Yii::$app->session;
            $session->set('token', $api->response['data']['token']);
            $session->set('status', $api->response['data']['status']);
			
            return new static($api->response['data']);
			
        }

        return null;
		
    }

    /**
     * @inheritdoc
     */
    public function getId()
	{
        return $this->id;
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
	{
        return $this->authKey;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
	{
        return $this->authKey === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
	{
        return $this->password === $password;
    }

    /*  REMOVE THIS CODE - API WILL TAKE CARE ABOUT THE OTP CODE */

    public function generatePasswordResetToken()
	{
        $this->otp = \Yii::$app->getSecurity()->generateRandomString() . '_' . time();
    }

    public function removePasswordResetToken()
	{
        $this->otp = null;
    }

    /* END REMOVE */
}