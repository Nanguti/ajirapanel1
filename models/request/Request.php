<?php
namespace app\models\request;

use Yii;
use yii\base\Model;
use yii\data\ArrayDataProvider;

//models

class Request extends Model
{

	public $id;
	public $job_id;
	public $customerName;
	public $categoryName;
	public $serviceName;
	public $location;
	public $status;
	public $cdate;
	public $totalPros;

    public function rules()
    {
		return [
			//list
			['id','integer','on'=>'list'],

			['job_id', 'trim','on'=>'list'],
			['job_id', 'string', 'length' => [0,8],'on'=>'list'],

			['categoryName', 'trim','on'=>'list'],
			['categoryName', 'string', 'length' => [0,128],'on'=>'list'],
			
			['customerName', 'trim','on'=>'list'],
			['customerName', 'string', 'length' => [0,128],'on'=>'list'],

			['cdate', 'trim','on'=>'list'],
			['cdate', 'string', 'length' => [0,128],'on'=>'list'],

			['serviceName', 'trim','on'=>'list'],
			['serviceName', 'string', 'length' => [0,128],'on'=>'list'],

			['status', 'trim','on'=>'list'],
			['status', 'string', 'length' => [0,20],'on'=>'list'],
			//detail
			['id', 'integer', 'on'=>'detail'],

		];
    }

    /**
     */

    public function requestList($data)
    {

		$data = $this->filterLikeArray('job_id',$this->job_id,$data);
		$data = $this->filterLikeArray('customerName',$this->customerName,$data);
		$data = $this->filterLikeArray('serviceName',$this->serviceName,$data);
		$data = $this->filterLikeArray('status',$this->status,$data);
		$data = $this->filterLikeArray('cdate',$this->cdate,$data);

		$arrayDataProvider = new ArrayDataProvider([
			'allModels'=>$data,
			'pagination'=>[
				'pageSize'=>Yii::$app->params['pagination'],
			],
			'sort' => [
				'attributes'=>['job_id','customerName','serviceName','status','cdate'],
				'defaultOrder'=>['cdate'=>SORT_DESC],
			],
		]);

		return $arrayDataProvider;

    }

    public function requestDetail($data)
    {
		$arrayDataProvider['request'] = new ArrayDataProvider([
			'allModels'=>$data['request'],
			'pagination'=>[
				'pageSize'=>Yii::$app->params['pagination'],
			],
		]);

		return $arrayDataProvider;

    }

	private function filterLikeArray($item,$search,$data,$caseSensitive=0)
    {

		if(!is_array($this->$item) && !is_array($search) && $search && $this->$item){
			foreach($data as $i=>$v){
				if($caseSensitive){
					if(strpos($v[$item],$search)===false){
						unset($data[$i]);
					}
				}else{
					if(strpos(strtolower($v[$item]),strtolower($search))===false){
						unset($data[$i]);
					}
				}
			}
		}

		return $data;

	}
	
}
