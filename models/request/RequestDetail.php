<?php
namespace app\models\request;

use Yii;
use yii\base\Model;
use yii\data\ArrayDataProvider;

class RequestDetail extends Model
{

	public $id;
	public $job_id;
	public $customerName;
	public $categoryName;
	public $serviceName;
	public $location;
	public $status;
	public $cdate;
	public $totalPros;

    public function rules()
    {
        return [
			['id', 'integer', 'on'=>'detail'],
        ];
    }

    public function requestDetail($data)
    {

		$arrayDataProvider['request'] = $data['request'];

		$arrayDataProvider['questions'] = new ArrayDataProvider([
			'allModels'=>$data['questions'],
			'pagination'=>[
				'pageSize'=>Yii::$app->params['pagination'],
			],
		]);


		$arrayDataProvider['providers'] = new ArrayDataProvider([
			'allModels'=>$data['providers'],
			'pagination'=>[
				'pageSize'=>Yii::$app->params['pagination'],
			],
		]);

		return $arrayDataProvider;

    }

	private function filterLikeArray($item,$search,$data,$caseSensitive=0)
    {
		
		if(!is_array($this->$item) && !is_array($search) && $search && $this->$item){
			foreach($data as $i=>$v){
				if($caseSensitive){
					if(strpos($v[$item],$search)===false){
						unset($data[$i]);
					}
				}else{
					if(strpos(strtolower($v[$item]),strtolower($search))===false){
						unset($data[$i]);
					}
				}
			}
		}
		return $data;
		
	}

}
