<?php
namespace app\models\customer;
use Yii;
use yii\base\Model;
use yii\data\ArrayDataProvider;

class CustomerDetail extends Model
{

	public $iUserId;
	public $vFirstName;
	public $vLastName;
	public $gender;
	public $eStatus;
	public $mobileNumber;
	public $vEmail;
	public $dob;
	public $vLatitude;
	public $vLongitude;
	public $cdate;
	public $vUserPhoto;
	public $occupation;
	public $website;
	public $education;
	public $experience;
	public $aboutYourself;
	public $bio;
	public $udate;
	public $cancelledRequests;
	public $acceptedRequests;
	public $pendingRequests;
	public $progressRequests;
	public $deletedRequests;
	public $completedRequests;
	public $unavailableRequests;


    public function rules()
    {
        return [
			['iUserId', 'integer', 'on'=>'detail'],
        ];
    }

    public function customerDetail($data)
    {
		$arrayDataProvider['customer'] = $data['customer'];
		$arrayDataProvider['status_count'] = $data['status_count'];
		$arrayDataProvider['details'] = $data['details'];

		return $arrayDataProvider;
    }
	
	public function attributeLabels()
	{
	return array(
		'iUserId' => 'Customer ID',
		'vFirstName' => 'First Name',
		'vLastName' => 'Last Name',
		'businessName' => 'Business Name',
		'vEmail' => 'Email',
		'dob' => 'Date of Birth',
		'vLongitude'=>'Latitude',
		'vLongitude'=> 'Longitude',
		

	);
	}

	private function filterLikeArray($item,$search,$data,$caseSensitive=0)
    {
		if(!is_array($this->$item) && !is_array($search) && $search && $this->$item){
			foreach($data as $i=>$v){
				if($caseSensitive){
					if(strpos($v[$item],$search)===false){
						unset($data[$i]);
					}
				}else{
					if(strpos(strtolower($v[$item]),strtolower($search))===false){
						unset($data[$i]);
					}
				}
			}
		}
		return $data;
		
	}
}
