<?php
namespace app\models\customer;

use Yii;
use yii\base\Model;
use yii\data\ArrayDataProvider;

class Customer extends Model
{

	public $iUserId;
	public $vFirstName;
	public $vLastName;
	public $gender;
	public $eStatus;
	public $mobileNumber;
	public $vEmail;
	public $dob;
	public $approval_status;


    public function rules()
    {
        return [
			['vFirstName', 'trim','on'=>'list'],
			['vFirstName', 'string', 'length' => [0,128],'on'=>'list'],
			['vLastName', 'trim','on'=>'list'],
			['iUserId', 'trim', 'on'=>'list'],
			['vLastName', 'string', 'length' => [0,255],'on'=>'list'],
			['eStatus', 'trim','on'=>'list'],
			['eStatus', 'string', 'length' => [0,20],'on'=>'list'],
			['mobileNumber', 'trim','on'=>'list'],
			['mobileNumber', 'string', 'length' => [0,20],'on'=>'list'],
			['gender', 'trim','on'=>'list'],
			['gender', 'string', 'length' => [0,20],'on'=>'list'],
			['vEmail', 'trim','on'=>'list'],
			['vEmail', 'string', 'length' => [0,20],'on'=>'list'],
			['dob', 'trim','on'=>'list'],
			['dob', 'string', 'length' => [0,20],'on'=>'list'],
			['approval_status', 'trim','on'=>'list'],
			['approval_status', 'string', 'length' => [0,20],'on'=>'list'],
			//detail
			['iUserId', 'integer', 'on'=>'detail'],

        ];
    }

    public function customerList($data)
    {

		$data = $this->filterLikeArray('iUserId',$this->iUserId,$data);
		$data = $this->filterLikeArray('vFirstName',$this->vFirstName,$data);
		$data = $this->filterLikeArray('vLastName',$this->vLastName,$data);
		$data = $this->filterLikeArray('mobileNumber',$this->mobileNumber,$data);
		$data = $this->filterLikeArray('gender',$this->gender,$data);
		$data = $this->filterLikeArray('vEmail',$this->vEmail,$data);
		$data = $this->filterLikeArray('dob',$this->dob,$data);
		$data = $this->filterLikeArray('eStatus',$this->eStatus,$data);
		$data = $this->filterLikeArray('approval_status',$this->approval_status,$data);

		$arrayDataProvider = new ArrayDataProvider([
			'allModels'=>$data,
			'pagination'=>[
				'pageSize'=>Yii::$app->params['pagination'],
			],
			'sort' => [
				'attributes'=>[
					'iUserId',
					'vFirstName',
					'vLastName',
					'eStatus',
					'mobileNumber',
					'gender',
					'vEmail',
					'dob',
					'approval_status',
					],
				'defaultOrder'=>['iUserId'=>SORT_DESC],
			],
		]);

		return $arrayDataProvider;

    }

	private function filterLikeArray($item,$search,$data,$caseSensitive=0)
	{
		if(!is_array($this->$item) && !is_array($search) && $search && $this->$item){
			foreach($data as $i=>$v){
				if($caseSensitive){
					if(strpos($v[$item],$search)===false){
						unset($data[$i]);
					}
				}else{
					if(strpos(strtolower($v[$item]),strtolower($search))===false){
						unset($data[$i]);
					}
				}
			}
		}

		return $data;
	}

}