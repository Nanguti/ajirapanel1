<?php
namespace app\models\common;

use Yii;

use Aws\S3\S3Client;
use Aws\S3\Exception\S3Exception;
use yii\base\Component;

/**
 * Class Storage
 *
 * @package app\models\common
 */
class Storage extends Component
{

    /**
     * @var
     */
    private $_s3;

    /**
     * Return S3 Client instance
     *
     * @return S3Client
     */
    private function getInstance()
    {

        if ($this->_s3 === null) {
            $this->_s3 = $this->connect();
        }

        return $this->_s3;

    }

    /**
     * Instance the S3 object
     */
    public function connect()
    {

        $this->_s3 = S3Client::factory([
            'credentials' => [
                'key'    => Yii::$app->params['aws']['s3']['accesskey'],
                'secret' => Yii::$app->params['aws']['s3']['secret'],
            ],
            'version'     => Yii::$app->params['aws']['s3']['version'],
            'region'      => Yii::$app->params['aws']['s3']['region'],
            'bucket'      => Yii::$app->params['aws']['s3']['bucket'],
        ]);

        return $this->_s3;

    }

    /**
     * Upload file to S3
     *
     * @param $tempFile
     * @param $filePathName
     *
     * @return bool
     * @internal param string $fileName uploaded file instance
     * @internal param null $bucket bucket name. By default use bucket from config
     */
    public function upload($tempFile, $filePathName)
    {

        if (!empty($tempFile)) {

            $s3 = $this->getInstance();

            $upload = $s3->putObject([
                'Bucket' => Yii::$app->params['aws']['s3']['bucket'],
                'Key'    => $filePathName,
                'Body'   => file_get_contents($tempFile),
                'ACL'    => Yii::$app->params['aws']['s3']['acl'],
            ]);

            return true;

        }

        return false;

    }

    /**
     * @param $content
     * @param $filePathName
     *
     * @return bool
     */
    public function uploadContent($content, $filePathName)
    {

        if (!empty($tempFile)) {

            $s3 = $this->getInstance();

            $upload = $s3->putObject([
                'Bucket' => Yii::$app->params['aws']['s3']['bucket'],
                'Key'    => $filePathName,
                'Body'   => $content,
                'ACL'    => Yii::$app->params['aws']['s3']['acl'],
            ]);

            return true;

        }

        return false;

    }

    /**
     * @param $filePathName
     *
     * @return bool
     */
    public function deleteObject($filePathName)
    {

        if (!empty($filePathName)) {

            $s3 = $this->getInstance();

            $upload = $s3->deleteObject([
                'Bucket' => Yii::$app->params['aws']['s3']['bucket'],
                'Key'    => $filePathName,
            ]);

            return true;

        }

        return false;

    }

    /**
     * Check and create folder in S3 if not exist
     *
     * @param $path
     * @param bool $create
     *
     * @return bool
     * @internal param string $folderName folder name
     * @internal param null $bucket bucket name. By default use bucket from config
     */
    public function check($path, $create = false)
    {

        $s3 = $this->getInstance();
        $userFolder = $s3->doesObjectExist(Yii::$app->params['aws']['s3']['bucket'], $path);

        if (!$userFolder) {

            //used to create folder
            if ($create) {

                $upload = $s3->putObject([
                    'Bucket' => Yii::$app->params['aws']['s3']['bucket'],
                    'Key'    => $path,
                    'Body'   => '',
                    'ACL'    => Yii::$app->params['aws']['s3']['acl'],
                ]);

                return true;

            }

            return false;

        }

        return true;

    }

    /**
     * Get object secured url
     */
    public function urlSecured($path)
    {

        $s3 = $this->getInstance();

        $cmd = $s3->getCommand('GetObject', [
            'Bucket' => Yii::$app->params['aws']['s3']['bucket'],
            'Key'    => $path,
        ]);

        $request = $s3->createPresignedRequest($cmd, Yii::$app->params['aws']['s3']['expirationTime']);

        return (string)$request->getUri();

    }

    /**
     * @param $path
     *
     * @return mixed
     */
    public static function secured($path)
    {
        $s3 = new static();

        return $s3->urlSecured($path);
    }

}