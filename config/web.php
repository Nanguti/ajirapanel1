<?php

$params = require __DIR__ . '/params.php';
$params['API'] = require __DIR__ . '/api.php';
$db = require __DIR__ . '/db.php';

$config = [
    'id' => 'basic',
    'name' => 'Ajira Connect',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'bu4aT6c4cHa2AEZePh8KrURehudUtiAp',
        ],



        /*'cache' => [
            'class' => 'yii\caching\FileCache',
        ],*/
        'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => false,
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
    	'formatter' => [
    		'defaultTimeZone' => 'Africa/Nairobi',
    		'timeZone' => 'Africa/Nairobi',
    		'dateFormat' => 'php:d-m-Y',
    		'datetimeFormat'=>'php:d-M-Y H:i:s'
    	],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
				[
				'class' => 'yii\log\SyslogTarget',
				'levels' => ['info'],
				'facility' => '17',
				'except' => [
					'yii\db\*',
				],
				'maskVars' => ['_SERVER.HTTP_AUTHORIZATION','_SERVER.PHP_AUTH_USER','_SERVER.PHP_AUTH_PW','_POST.password','_POST.uuid','_POST.token'],
			],
			[
				'class' => 'yii\log\SyslogTarget',
				'levels' => ['error','warning'],
				'facility' => '17',
				'maskVars' => ['_SERVER.HTTP_AUTHORIZATION','_SERVER.PHP_AUTH_USER','_SERVER.PHP_AUTH_PW','_POST.password','_POST.uuid','_POST.token'],
			], 				
            ],
        ],
        //'db' => $db,
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => array(
                '<controller:\w+>/<id:\d+>' => '<controller>/view',
                '<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
                '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
            ),
        ],
    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];
} else {

	//$config['runtimePath'] = '@app/runtime/'. getHostByName(getHostName());

}

return $config;
