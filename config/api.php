<?php
//access to this vars using Yii::$app->params['varname']

return [

	'url' => $params['apiurl'],
	'connectTimeout' => 30000,
	'timeout' => 50000,
	'retry' => 2,
	'defaultMethod' => 'POST',
	'encode64' => false,
	'accessKey' => $params['accessKey'],
	'useragent' => 'cURL Request',

	'call' => [
		'info/api' => [
			'url' => 1,
			'method' => 'POST',
			'validateAccessKey' => true,
			'validateToken' => false,
		],
		'admin/auth' => [
			'url' => 1,
			'method' => 'POST',
			'validateAccessKey' => true,
			'validateToken' => false,
		],
		'admin/details' => [
			'url' => 1,
			'method' => 'POST',
			'validateAccessKey' => true,
			'validateToken' => true,
		],
		'requests/list' => [
			'url' => 1,
			'method' => 'POST',
			'validateAccessKey' => true,
			'validateToken' => true,
		],
		'requests/detail' => [
			'url' => 1,
			'method' => 'POST',
			'validateAccessKey' => true,
			'validateToken' => true,
		],
		'professionals/list' => [
			'url' => 1,
			'method' => 'POST',
			'validateAccessKey' => true,
			'validateToken' => true,
		],
		'professionals/detail' => [
			'url' => 1,
			'method' => 'POST',
			'validateAccessKey' => true,
			'validateToken' => true,
		],
		'professionals/activate' => [
			'url' => 1,
			'method' => 'POST',
			'validateAccessKey' => true,
			'validateToken' => true,
		],
		'professionals/edit' => [
			'url' => 1,
			'method' => 'POST',
			'validateAccessKey' => true,
			'validateToken' => true,
		],
		'customers/list' => [
			'url' => 1,
			'method' => 'POST',
			'validateAccessKey' => true,
			'validateToken' => true,
		],
		'customers/detail' => [
			'url' => 1,
			'method' => 'POST',
			'validateAccessKey' => true,
			'validateToken' => true,
		],
		'services/list' => [
			'url' => 1,
			'method' => 'POST',
			'validateAccessKey' => true,
			'validateToken' => true,
		],
		'services/list' => [
			'url' => 1,
			'method' => 'POST',
			'validateAccessKey' => true,
			'validateToken' => true,
		],
	],

];
